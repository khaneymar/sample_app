class AddCarModelToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :car_model, :string
  end
end
